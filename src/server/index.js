const csv = require('csvtojson');
const fs = require("fs");
const noOfMatchesPlayedPerCity = require('./noOfMatchesPlayedPerCity.js');
const noOfMatchesWonPerTeam = require('./noOfMatchesWonPerTeam.js');
const noOfRedCardsIssuedPerTeam = require('./noOfRedCardsIssuedPerTeam.js');
const getTop10Players = require('./top10PlayersHIghest.js');

const matchesFilePath = '/home/akash/Desktop/fifa-project/src/data/WorldCupMatches.csv';
const playersFilePath= '/home/akash/Desktop/fifa-project/src/data/WorldCupPlayers.csv';
const WorldCupsFilePath= '/home/akash/Desktop/fifa-project/src/data/WorldCups.csv';


csv()
  .fromFile(matchesFilePath)
  .then((matches) => {
    //1.
    fs.writeFile("/home/akash/Desktop/fifa-project/src/public/output/noOfMatchesPlayedPerCity.json",
    JSON.stringify(noOfMatchesPlayedPerCity(matches)), function (err) {
      if (err) {
        console.log("Error writing JSON file:", err);
      } else {
        console.log("JSON file created successfully!");
      }
    } );
    //3.
    csv()
    .fromFile(playersFilePath)
    .then((players) =>
    {
       fs.writeFile(
        "/home/akash/Desktop/fifa-project/src/public/output/noOfRedCardsIssuedPerTeam.json",
        JSON.stringify(noOfRedCardsIssuedPerTeam(matches,players,"2014")),
          "utf8",
          function (err) {
            if (err) {
              console.log("Error writing JSON file:", err);
            } else {
              console.log("JSON file created successfully!");
            }
          } );
        })
      });
 //4.
csv()
  .fromFile(playersFilePath)
  .then((players) => {
     fs.writeFile("/home/akash/Desktop/fifa-project/src/public/output/top10PlayersHighest.json",
     JSON.stringify(getTop10Players(players)), function (err) {
      if (err) {
        console.log("Error writing JSON file:", err);
      } else {
        console.log("JSON file created successfully!");
      }
    } )
  });
//2
csv().fromFile(matchesFilePath).then( (matches) => {
  fs.writeFile("/home/akash/Desktop/fifa-project/src/public/output/noOfMatchesWonPerTeam.json",
    JSON.stringify(noOfMatchesWonPerTeam(matches)),function (err) {
      if (err) {
        console.log("Error writing JSON file:", err);
      } else {
        console.log("JSON file created successfully!");
      }
    } );}
    )