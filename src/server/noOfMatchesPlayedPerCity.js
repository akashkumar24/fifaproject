function noOfMatchesPlayedPerCity(matches){
 const cities = matches.map((match)=> match.City);
 const matchesPerCity = cities.reduce((acc, city) => {
    acc[city] = acc[city] ? acc[city] + 1 : 1;
    return acc;
  }, {});
      return matchesPerCity;
}
module.exports = noOfMatchesPlayedPerCity;