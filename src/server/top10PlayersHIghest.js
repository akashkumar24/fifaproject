function getTop10Players(players) {
  const goalsByPlayer = players.reduce((goals, player) => {
    const numGoals = player["Event"].split("G").length - 1;
    goals[player["Player Name"]] = (goals[player["Player Name"]] || 0) + numGoals;
    return goals;
  }, {});

  const numGamesByPlayer = players.reduce((numGames, player) => {
    numGames[player["Player Name"]] = (numGames[player["Player Name"]] || 0) + 1;
    return numGames;
  }, {});

  const goalProbabilityByPlayer = Object.entries(goalsByPlayer).reduce((probabilities, [name, goals]) => {
    probabilities[name] = goals / numGamesByPlayer[name];
    return probabilities;
  }, {});

  return Object.fromEntries(Object.entries(goalProbabilityByPlayer)
    .sort(([, a], [, b]) => b - a)
    .slice(0, 10));
}

module.exports = getTop10Players;
