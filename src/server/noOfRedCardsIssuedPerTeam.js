function noOfRedCardsIssuedPerTeam(matches, players, year) {
  const matches2014 = matches.filter(match => match.Year === year);
  const matchIDs2014 = new Set(matches2014.map(match => match.MatchID));
  
  const result = players.reduce((acc, player) => {
    const id = player["MatchID"];
    const team = player["Team Initials"];
    const redCard = (player["Event"].match(/R/g) || []).length;
    if (matchIDs2014.has(id) && redCard) {
      acc.set(team, (acc.get(team) || 0) + 1);
    }
    return acc;
  }, new Map());
  
  return Object.fromEntries(result);
}

module.exports = noOfRedCardsIssuedPerTeam;
