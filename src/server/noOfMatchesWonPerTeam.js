function noOfMatchesWonPerTeam(matches) {
  const matchesWonPerTeam = matches.reduce((acc, match) => {
    const homeTeamGoals = match['Home Team Goals'];
    const awayTeamGoals = match['Away Team Goals'];
    const homeTeamName = match['Home Team Name'];
    const awayTeamName = match['Away Team Name'];

    if (homeTeamGoals > awayTeamGoals) {
      acc[homeTeamName] = acc[homeTeamName] ? acc[homeTeamName] + 1 : 1;
    } else if (homeTeamGoals < awayTeamGoals) {
      acc[awayTeamName] = acc[awayTeamName] ? acc[awayTeamName] + 1 : 1;
    }

    return acc;
  }, {});

  return matchesWonPerTeam;
}

module.exports = noOfMatchesWonPerTeam;
